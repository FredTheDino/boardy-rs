use crate::msg::Msg;
use crate::model::Model;
use fzcmd::{Command, ParamGreed, RestQuery};
use gitlab::IssueState;

use std::sync::Arc;


pub fn get_parser(model: &Model) -> Command<Msg> {
    /*
    fn single_word(
        suggestions: Vec<String>,
        rest_command: Box<dyn Fn(&str) -> Option<Command<Msg>>>
    ) -> Option<Command<Msg>> {
        Some(Command::NonTerminal(
                ParamGreed::Word,
                suggestions,
                Box::new(move |query, _| {
                    rest_command(query)
                })
            ))
    }
    */

    fn single_arg(
        suggestions: Vec<String>,
        rest_command: Box<dyn Fn(&str) -> Option<Command<Msg>>>
    ) -> Option<Command<Msg>> {
        Some(Command::NonTerminal(
                ParamGreed::ToComma,
                suggestions,
                Box::new(move |query, _: RestQuery| {
                    rest_command(query)
                })
            ))
    }


    fn comma_separated_list<T: Clone>(
        current: Vec<String>,
        suggestions: Vec<String>,
        extra_payload: Arc<T>,
        rest_command: &'static dyn Fn(T, Vec<String>) -> Option<Command<Msg>>
    ) -> Option<Command<Msg>> {
        Some(Command::NonTerminal(
                ParamGreed::ToComma,
                suggestions.clone(),
                Box::new(move |query, rest| {
                    let mut current = current.clone();
                    current.push(query.to_string());
                    if rest.is_empty() {
                        rest_command((*extra_payload).clone(), current)
                    }
                    else {
                        comma_separated_list(
                            current,
                            suggestions.clone(),
                            extra_payload.clone(),
                            rest_command,
                        )
                    }
                })
            ))
    }

    fn add_labels(issue: String, labels: Vec<String>) -> Option<Command<Msg>> {
        Some(Command::Terminal(Msg::AddLabel(issue, labels)))
    }
    fn remove_labels(issue: String, labels: Vec<String>) -> Option<Command<Msg>> {
        Some(Command::Terminal(Msg::RemoveLabels(issue, labels)))
    }
    fn create_issue(issue: String, labels: Vec<String>) -> Option<Command<Msg>> {
        Some(Command::Terminal(Msg::CreateIssue(issue, labels)))
    }

    let issues = model.issues.iter()
        .map(|l| l.title.clone())
        .collect::<Vec<_>>();
    let labels = model.labels.iter().map(|(l, _)| l.clone()).collect::<Vec<_>>();
   
    let open_issues = model.issues.iter().cloned()
        .filter(|f| f.state != IssueState::Closed)
        .map(|i| i.title)
        .collect::<Vec<_>>();

    let closed_issues = model.issues.iter().cloned()
        .filter(|f| f.state == IssueState::Closed)
        .map(|i| i.title)
        .collect::<Vec<_>>();

    let label_labels = model.labels.keys()
        .filter(|l| !model.board_labels.contains(l))
        .cloned()
        .collect::<Vec<_>>();
    let board_labels = model.board_labels.clone();

    Command::NonTerminal(
        ParamGreed::Word,
        vec!(
            "quit".into(),
            "exit".into(),
            "add_label".into(),
            "remove_labels".into(),
            "edit_description".into(),
            "create_issue".into(),
            "open_issue".into(),
            "close_issue".into(),
            "mark_issue".into(),
        ),
        Box::new(move |query, _| {
            match query {
                "quit" => {
                    Some(Command::Terminal(Msg::Exit))
                }
                "exit" => {
                    Some(Command::Terminal(Msg::Exit))
                },
                // TODO: Deduplicate the duplication between
                // add and remove_labels
                "add_label" => {
                    let labels = label_labels.clone();
                    single_arg(
                        issues.clone(),
                        Box::new(move |word| {
                            comma_separated_list(
                                vec!(),
                                labels.clone(),
                                Arc::new(word.to_string()),
                                &add_labels
                            )
                        })
                    )
                }
                "remove_labels" => {
                    let labels = label_labels.clone();
                    single_arg(
                        issues.clone(),
                        Box::new(move |word| {
                            comma_separated_list(
                                vec!(),
                                labels.clone(),
                                Arc::new(word.to_string()),
                                &remove_labels
                            )
                        })
                    )
                }
                "edit_description" => {
                    single_arg(
                            issues.clone(),
                            Box::new(move |word| {
                                Some(Command::Terminal(
                                    Msg::StartEditDescription(word.into())
                                ))
                            })
                        )
                }
                "create_issue" => {
                    let labels = labels.clone();
                    single_arg(
                        vec!(),
                        Box::new(move |word| {
                            comma_separated_list(
                                vec!(),
                                labels.clone(),
                                Arc::new(word.to_string()),
                                &create_issue
                            )
                        })
                    )
                }
                "close_issue" => {
                    single_arg(
                        open_issues.clone(),
                        Box::new(move |query| {
                            Some(Command::Terminal(
                                Msg::CloseIssue(query.into())
                            ))
                        })
                    )
                }
                "open_issue" => {
                    single_arg(
                        closed_issues.clone(),
                        Box::new(move |query| {
                            Some(Command::Terminal(
                                Msg::OpenIssue(query.into())
                            ))
                        })
                    )
                }
                "mark_issue" => {
                    let mut board_labels = board_labels.clone();
                    board_labels.push("open".into());
                    single_arg(
                        issues.clone(),
                        Box::new(move |query| {
                            let issue = query.to_string();
                            single_arg(
                                board_labels.clone(),
                                Box::new(move |new_issue| {
                                    let new_issue =
                                        if new_issue != "open" {new_issue} else {""};
                                    Some(Command::Terminal(
                                        Msg::ModifyIssueList(issue.clone(), new_issue.into())
                                    ))
                                })
                            )
                        })
                    )
                }
                _ => None
            }
        })
    )
}

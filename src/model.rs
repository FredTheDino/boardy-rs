use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::io::Stdin;

use gitlab::{Issue, IssueState, IssueInternalId};

use tui::style::{Color};
use termion::input::Keys;

use fzcmd::{FuzzyOutput, expand_command, parse_command, ParseError};
use crate::commands::get_parser;
use crate::msg::{Cmd};

pub type CommandLineState = birch::line_input::CommandLineState<ParseError>;


pub struct Model {
    /// Current command line input
    pub cli: Option<CommandLineState>,
    /// Available labels
    pub labels: HashMap<String, Label>,
    /// Available issues
    pub issues: Vec<Issue>,
    /// List of issues which are expected to be updated
    /// by the server as a response from a request
    pub issues_waiting_for_update: Vec<IssueInternalId>,
    /// Available boards as a list of issues
    pub board_labels: Vec<String>,
    /// ID of the current project
    pub project_id: gitlab::ProjectId,
    /// The user associated with the gitlab token
    pub current_user: gitlab::UserPublic,
    /// Current scroll level as line offset from the top
    pub scroll_level: u16,
    /// List of errors that have occured
    pub errors: Vec<String>,
    /// Lock around stdin.
    /// Used to stop the key event loop whenever an external editor is open
    pub stdin_keys: Arc<Mutex<Keys<Stdin>>>,
}

impl Model {
    pub fn new(
        project_id: gitlab::ProjectId,
        board_labels: Vec<String>,
        current_user: gitlab::UserPublic,
        stdin_keys: Arc<Mutex<Keys<Stdin>>>,
    ) -> Self {
        Model {
            issues: Vec::new(),
            issues_waiting_for_update: Vec::new(),
            labels: HashMap::new(),
            project_id,
            board_labels,
            current_user,
            cli: Default::default(),
            scroll_level: 0,
            errors: Vec::new(),
            stdin_keys,
        }
    }
}

impl Model {
    pub fn set_cli_input(mut self, input: Option<String>) -> Self {
        match (&mut self.cli, &input) {
            (Some(cli), Some(input)) => {
                cli.current_input = input.clone()
            },
            (Some(_), None) => self.cli = None,
            (None, Some(input)) => {
                self.cli = Some(CommandLineState {
                    current_input: input.clone(),
                    .. Default::default()
                })
            }
            (None, None) => {}
        }

        self.run_fuzzy_parser()
    }

    fn set_cli(self, new: Option<CommandLineState>) -> Self {
        Self {cli: new, .. self}
    }


    fn run_fuzzy_parser(self) -> Self {
        let parser = get_parser(&self);
        match &self.cli {
            Some(ref cli) => {
                let FuzzyOutput {expanded, suggestions} =
                    expand_command(&cli.current_input, parser);

                let parsed = parse_command(&expanded, get_parser(&self));

                let new_cli = CommandLineState {
                    fuzzy_expansion: expanded,
                    fuzz_suggestions: suggestions.unwrap_or_else(|_| vec!()),
                    last_error: parsed.err(),
                    .. cli.clone()
                };
                self.set_cli(Some(new_cli))
            }
            None => self
        }
    }

    pub fn accept_input(self) -> (Self, Vec<Cmd>) {
        let parser = crate::commands::get_parser(&self);
        match self.cli {
            None => panic!("Accepting CLI input without CLI activated"),
            Some(ref cli) => {
                match parse_command(&cli.fuzzy_expansion, parser) {
                    Ok(msg) => {
                        (self.set_cli_input(None), vec!(Cmd::Loopback(msg)))
                    }
                    Err(e) => {
                        let new_cli = CommandLineState{
                            last_error: Some(e),
                            ..cli.clone()
                        };
                        (self.set_cli(Some(new_cli)), vec!())
                    }
                }
            }
        }
    }

    pub fn set_issues(self, issues: Vec<Issue>) -> Self {
        Self {issues, .. self}
    }
    pub fn update_issue(self, issue: Issue) -> Self {
        let id = issue.id;
        let iid = issue.iid;
        Self {
            issues: self.issues.iter()
                .filter(|i| i.id != id)
                .chain(&[issue])
                .cloned()
                .collect(),
            issues_waiting_for_update: self.issues_waiting_for_update.into_iter()
                .filter(|x| *x != iid)
                .collect(),
            .. self
        }
    }
    pub fn set_labels(self, labels: Vec<gitlab::Label>) -> Self {
        let labels = labels.into_iter()
            .map(|l| (l.name.clone(), l.into()))
            .collect::<HashMap<_, _>>();

        Self {labels, .. self}
    }

    pub fn try_update<A>(
        self,
        updater: impl Fn(Self, A) -> Self,
        value: Result<A, String>
    ) -> Self {
        match value {
            Ok(val) => updater(self, val),
            Err(e) => {
                self.with_added_error(e)
            }
        }
    }

    pub fn with_added_error(mut self, err: String) -> Self {
        self.errors.push(err);
        self
    }


    pub fn boards(&self) -> Vec<(String, Vec<Issue>)> {
        let (mut remaining, closed) = self.issues.clone().into_iter()
            .fold((vec!(), vec!()), |(mut acc_r, mut acc_c), issue| {
                if issue.state == IssueState::Closed {
                    acc_c.push(issue);
                }
                else {
                    acc_r.push(issue);
                }
                (acc_r, acc_c)
            });

        let mut result = vec!();
        for label in &self.board_labels {
            let (remaining_, here) = remaining.into_iter()
                .fold((vec!(), vec!()), |(mut acc_r, mut acc_h), issue| {
                    if issue.labels.contains(&label) {
                        acc_h.push(issue)
                    }
                    else {
                        acc_r.push(issue)
                    }
                    (acc_r, acc_h)
                });
            remaining = remaining_;
            result.push((label.clone(), here))
        }
        result.insert(0, ("Open".into(), remaining));
        result.push(("Closed".into(), closed));
        result
    }
}


pub struct Label {
    // The inner value
    pub i: gitlab::Label,
    pub color: Color,
}

fn parse_hex_color(value: &str) -> Color {
    let warn = |e| {warn!("Invalid color component: {}", e); 0};
    // Starts at one to get rid of #
    let r = u8::from_str_radix(&value[1..3], 16).unwrap_or_else(warn);
    let g = u8::from_str_radix(&value[3..5], 16).unwrap_or_else(warn);
    let b = u8::from_str_radix(&value[5..7], 16).unwrap_or_else(warn);
    Color::Rgb(r, g, b)
}

impl From<gitlab::Label> for Label {
    fn from(other: gitlab::Label) -> Self {
        let color = parse_hex_color(&other.color.clone().value());
        Self {
            i: other,
            color
        }
    }
}
